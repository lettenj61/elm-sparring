module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)

import Component.Frag as Frag

main : Program () Model Msg
main =
    Browser.sandbox
        { init = "index"
        , view = view
        , update = update
        }


type alias Model = String


type Msg
    = NewRoute String

update : Msg -> Model -> Model
update msg model =
    case msg of
        NewRoute route ->
            route


view : Model -> Html Msg
view model =
    div
        []
        [ p [] [ text "Elm sparring!" ]
        , Frag.button "" [] [ text "no effect" ]
        ]
