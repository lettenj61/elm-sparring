module Component.Frag exposing (..)

import List
import Html
import Html exposing (Html, Attribute)
import Html.Attributes as A

button : String -> List (Attribute msg) -> List (Html msg) -> Html msg
button modifiers attrs children =
    Html.button
        ( ( A.class "button is-primary" ) :: attrs )
        children
